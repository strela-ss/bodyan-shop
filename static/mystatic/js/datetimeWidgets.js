$(document).ready(function() {

    $('.myTimeField').bootstrapMaterialDatePicker({date: false, format: 'HH:mm'
    }).on('change', function(e, date) {
        $(this).attr('value', date.format('HH:mm:00'))
    });

    $('.myDateField').bootstrapMaterialDatePicker({weekStart : 1, time: false
    }).on('change', function(e, date) {
        $(this).attr('value', date.format('YYYY-MM-DD'))
    });

   //$.material.init();
});