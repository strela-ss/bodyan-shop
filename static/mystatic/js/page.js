var footer;

$(document).ready(function(){
	footer = $('#footer');
	if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        footer.removeClass("hide");
    }
});

window.onscroll = function(ev) {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        footer.removeClass("hide");
    } else {
    	if(!footer.hasClass("hide")) footer.addClass("hide");
    }
};