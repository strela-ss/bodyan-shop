# coding=utf-8
from __future__ import unicode_literals
import django.utils.timezone
from django.contrib.auth.models import User

from django.db import models
from band.models import Band
from housing.models import Housing

CART_STATUS = (
    ("OPEN", "Відкрито"),
    ("CLOSED", "Закрито"),
    ("PROGRESS", "В обробці"),
)


class Cart(models.Model):
    status = models.CharField(
        null=False,
        default="OPEN",
        max_length=100,
        choices=CART_STATUS,
    )
    user = models.ForeignKey(
        User,
        blank=False
    )
    bands = models.ManyToManyField(
        Band,
        blank=True
    )
    housings = models.ManyToManyField(
        Housing,
        blank=True
    )
    last_modified = models.DateTimeField(
        auto_now=True,
    )

    def sum(self):
        result = 0
        if self.bands.exists():
            for i in self.bands.all():
                result += i.price
        if self.housings.exists():
            for i in self.housings.all():
                result += i.price
        return result

    def __unicode__(self):
        return "Замовлення №%s, статус: %s, дата: %s" % (
            self.id,
            self.get_status_display(),
            self.last_modified.strftime("%d-%m-%Y")
        )
