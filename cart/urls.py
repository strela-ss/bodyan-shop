from django.conf.urls import url
import cart.views


urlpatterns = [
    url(r'^$', cart.views.index, name="index"),
    url(r'^remove-item/$', cart.views.remove_item, name="remove-item"),
    url(r'^confirm/(?P<id>\d+)/$', cart.views.confirm, name="confirm"),
]
