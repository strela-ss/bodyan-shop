from django.contrib import admin
from cart.forms import CartForm
from cart.models import Cart


class CartAdmin(admin.ModelAdmin):

    form = CartForm

    list_display = (
        'user',
        'status',
    )


admin.site.register(Cart, CartAdmin)
