# coding=utf-8
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from band.models import Band
from cart.models import Cart
from cart.utils import *
from housing.models import Housing


@login_required
def index(reuqest):
    user = reuqest.user
    bands = {}
    housings = {}
    if user.cart_set.filter(status="OPEN").exists():
        cart = user.cart_set.get(status="OPEN")
        bands = cart.bands.all()
        housings = cart.housings.all()
    elif user.cart_set.filter(status="PROGRESS").exists():
        cart = user.cart_set.get(status="PROGRESS")
        bands = cart.bands.all()
        housings = cart.housings.all()
    else:
        cart = None
    context = {
        "bands": bands,
        "housings": housings,
        "cart": cart,
    }
    return render(reuqest, "cart/index.html", context)


@csrf_exempt
def remove_item(request):
    user = request.user
    if not user.is_authenticated():
        return http_response_400(
            "403",
            "Permission denied",
        )
    if request.method == "POST":
        if 'id' not in request.POST or 'type' not in request.POST or 'cart' not in request.POST:
            return http_response_400(
                "404",
                "wrong params"
            )
        id = request.POST.get('id')
        type = request.POST.get('type')
        try:
            cart = Cart.objects.get(id=request.POST.get('cart'))
        except:
            return http_response_400("404", "no such cart")
        if cart.status in ['PROGRESS', 'CLOSED']:
            return http_response_400("403", "cart in progress or closed")

        if type == "band":
            if cart.bands.filter(id=id).exists():
                band = Band.objects.get(id=id)
                cart.bands.remove(band)
            else:
                return http_response_400(
                    "404",
                    "no band with id %s in your card" % id,
                )
        elif type == "housing":
            if cart.housings.filter(id=id).exists():
                housing = Housing.objects.get(id=id)
                cart.housings.remove(housing)
            else:
                return http_response_400(
                    "404",
                    "no housing with id %s in your card" % id,
                )
        return HttpResponse(
            json.dumps({
                'sum': str(cart.sum())
            }),
            content_type='application/json',
        )
    else:
        return get_400_response()


@login_required
def confirm(request, id):
    user = request.user
    if user.cart_set.filter(id=id).exists():
        cart = user.cart_set.get(id=id)
    else:
        raise HttpResponseForbidden(u"Замовлення з номером %s не знайдено" % id)
    if cart.status != "OPEN":
        raise HttpResponseForbidden(u"Замовлення з номером %s оброблюється або вже закрита" % id)
    cart.status = "PROGRESS"
    cart.save()
    return HttpResponseRedirect(reverse("profile:orders"))
