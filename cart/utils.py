import json
import datetime
from django.http import HttpResponse


def http_response_200(out_dict):
    return HttpResponse(
        json.dumps(out_dict),
        content_type='application/json',
    )


def http_response_400(status, value):
    response = HttpResponse(
        json.dumps(
            {
                'status': status,
                'reason': value
            }),
        content_type='application/json',
    )
    response.status_code = 400
    return response


def post_400_response():
    response = HttpResponse(json.dumps(
        {
            'status': '400',
            'reason': 'Use GET request'}
    ),
        content_type='application/json'
    )
    response.status_code = 400
    return response


def get_400_response():
    response = HttpResponse(json.dumps(
        {
            'status': '400',
            'reason': 'Use POST request'}
    ),
        content_type='application/json'
    )
    response.status_code = 400
    return response


def js_data_to_python(jsdata):
    splitted = jsdata.split(jsdata[4])
    return datetime.date(int(splitted[0]), int(splitted[1]), int(splitted[2]))


def python_data_to_js_data(pydata):
    return pydata.strftime("%Y/%m/%d")


def python_data_to_normal(pydata):
    return pydata.strftime("%d/%m/%Y")
