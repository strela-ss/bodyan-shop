from django import template
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter(name='band_logo_or_default')
def band_logo_or_default(url):
    if url:
        return url.url
    else:
        return static('img/default_band.jpg')


@register.filter(name='housing_logo_or_default')
def housing_logo_or_default(url):
    if url:
        return url.url
    else:
        return static('img/default_housing.jpg')
