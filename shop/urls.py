from django.conf.urls import include, url
from django.contrib import admin
import band.views


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^bands/', include("band.urls", namespace='band')),
    url(r'^housings/', include("housing.urls", namespace='housing')),
    url(r'^cart/', include("cart.urls", namespace='cart')),
    url(r'^profile/', include("myprofile.urls", namespace='profile')),

    url(r'^contacts/$', band.views.contact, name="contact"),
    url(r'^$', band.views.index, name="home"),
]
