from django.conf.urls import url
import myprofile.views


urlpatterns = [
    url(r'^$', myprofile.views.index, name="index"),
    url(r'^housings/$', myprofile.views.housings, name="housings"),
    url(r'^bands/$', myprofile.views.bands, name="bands"),
    url(r'^orders/$', myprofile.views.orders, name="orders"),

    url(r'^housings/new/$', myprofile.views.new_housing, name="new_housing"),
    url(r'^bands/new/$', myprofile.views.new_band, name="new_band"),
    url(r'^housings/(?P<id>\d+)/$', myprofile.views.update_housing, name="update_housing"),
    url(r'^bands/(?P<id>\d+)/$', myprofile.views.update_band, name="update_band"),

    url(r'^remove-housing/$', myprofile.views.remove_housing, name="remove-housing"),
    url(r'^remove-band/$', myprofile.views.remove_band, name="remove-band"),

    url(r'^orders/$', myprofile.views.orders, name="orders"),
    url(r'^orders/(?P<id>\d+)/$', myprofile.views.order, name="order"),

    url(r'^admin/orders/$', myprofile.views.admin_order_listing, name="admin_order_listing"),
    url(r'^admin/orders/(?P<id>\d+)/$', myprofile.views.admin_order_details, name="admin_order_details"),
    url(r'^admin/orders/approve/$', myprofile.views.admin_order_approve, name="approve_order"),
    url(r'^admin/users/$', myprofile.views.admin_users, name="admin_users"),
    url(r'^admin/users/grant/$', myprofile.views.admin_user_grant, name="admin_grant_staff"),
]
