# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from band.forms import BandUserForm
from band.models import Band, Actor
from cart.models import Cart
from housing.forms import HousingUserForm, HousingMapForm
from housing.models import Housing
from myprofile.utils import *


@login_required
def index(request):
    context = {

    }
    return render(request, "myprofile/index.html", context)


@login_required
def bands(request):
    query = request.user.band_set.all()
    if 'search' in request.GET:
        search = request.GET.get('search')
        query = query.filter(
            Q(name__icontains=search))
    paginator = Paginator(query.order_by("name"), 10)
    page = request.GET.get('page')
    try:
        bands = paginator.page(page)
    except PageNotAnInteger:
        bands = paginator.page(1)
        page = 1
    except EmptyPage:
        bands = paginator.page(paginator.num_pages)
        page = paginator.num_pages
    context = {
        "bands": bands,
        "pages": range(1, paginator.num_pages+1),
        "active_page": int(page),
    }
    return render(request, "myprofile/bands.html", context)


@login_required
def housings(request):
    query = request.user.housing_set.all()
    if 'search' in request.GET:
        search = request.GET.get('search')
        query = query.filter(
            Q(name__icontains=search))
    paginator = Paginator(query.order_by("name"), 10)
    page = request.GET.get('page')
    try:
        housings = paginator.page(page)
    except PageNotAnInteger:
        housings = paginator.page(1)
        page = 1
    except EmptyPage:
        housings = paginator.page(paginator.num_pages)
        page = paginator.num_pages
    context = {
        "housings": housings,
        "pages": range(1, paginator.num_pages+1),
        "active_page": int(page),
    }
    return render(request, "myprofile/housings.html", context)


@login_required
def orders(request):
    user = request.user
    context = {
        "carts": user.cart_set.all()
    }
    return render(request, "myprofile/orders.html", context)


@login_required
def new_housing(request):
    user = request.user
    if request.method == "POST":
        form = HousingMapForm(request.POST, request.FILES)
        if form.is_valid():
            housing = form.save(commit=False)
            housing.user = user
            housing.save()
            return HttpResponseRedirect(reverse("profile:housings"))
    else:
        form = HousingMapForm()
    context = {
        "form": form,
    }
    return render(request, "myprofile/new/housing.html", context)


@login_required
def update_housing(request, id):
    user = request.user
    housing = get_object_or_404(Housing, id=id)
    if request.method == "POST":
        form = HousingMapForm(request.POST, request.FILES, instance=housing)
        if form.is_valid():
            housing = form.save(commit=False)
            housing.user = user
            housing.save()
            return HttpResponseRedirect(reverse("profile:housings"))
    else:
        form = HousingMapForm(instance=housing)
    context = {
        "form": form,
    }
    return render(request, "myprofile/new/housing.html", context)


@login_required
def update_band(request, id):
    user = request.user
    band = get_object_or_404(Band, id=id)
    if request.method == "POST":
        form = BandUserForm(request.POST, request.FILES, instance=band)
        if form.is_valid():
            band = form.save(commit=False)
            band.user = user
            band.save()
            return HttpResponseRedirect(reverse("profile:bands"))
    else:
        form = BandUserForm(instance=band)
    context = {
        "form": form,
    }
    return render(request, "myprofile/new/housing.html", context)


@login_required
def new_band(request):
    user = request.user
    if request.method == "POST":
        form = BandUserForm(request.POST, request.FILES)
        if request.POST:
            if form.is_valid():
                band = form.save(commit=False)
                band.user = user
                band.save()
                return HttpResponseRedirect(reverse("profile:bands"))
    else:
        form = BandUserForm()
    context = {
        "form": form,
    }
    return render(request, "myprofile/new/band.html", context)


@csrf_exempt
def remove_housing(request):
    if request.method == "POST":
        user = request.user
        if 'id' not in request.POST:
            return http_response_400(
                "404",
                "wrong params"
            )
        id = request.POST.get("id")
        if user.housing_set.filter(id=id).exists():
            housing = Housing.objects.get(id=id)
        else:
            return http_response_400("404", "no such housing")
        housing.delete()
        return HttpResponse(status=200, )
    else:
        return get_400_response()


@csrf_exempt
def remove_band(request):
    if request.method == "POST":
        user = request.user
        if 'id' not in request.POST:
            return http_response_400(
                "404",
                "wrong params"
            )
        id = request.POST.get("id")
        if user.band_set.filter(id=id).exists():
            band = Band.objects.get(id=id)
        else:
            return http_response_400("404", "no such housing")
        band.delete()
        return HttpResponse(status=200, )
    else:
        return get_400_response()


@login_required
def orders(request):
    paginator = Paginator(request.user.cart_set.order_by("-id"), 10)
    page = request.GET.get('page')
    try:
        carts = paginator.page(page)
    except PageNotAnInteger:
        carts = paginator.page(1)
        page = 1
    except EmptyPage:
        carts = paginator.page(paginator.num_pages)
        page = paginator.num_pages
    context = {
        "carts": carts,
        "pages": range(1, paginator.num_pages+1),
        "active_page": int(page),
    }
    return render(request, "myprofile/orders.html", context)


@login_required
def order(request, id):
    cart = get_object_or_404(Cart, id=id, user_id=request.user.id)
    context = {
        "cart": cart,
    }
    return render(request, "myprofile/order.html", context)


@login_required
@user_passes_test(lambda u: u.is_staff or u.is_superuser)
def admin_order_listing(request):
    query = Cart.objects.filter(Q(status="CLOSED") | Q(status="PROGRESS"))
    if 'search' in request.GET:
        search = request.GET.get('search')
        query = query.filter(
            Q(user__username__icontains=search) |
            Q(user__first_name__icontains=search) |
            Q(user__email__icontains=search))
    paginator = Paginator(query.order_by("-last_modified"), 10)
    page = request.GET.get('page')
    try:
        carts = paginator.page(page)
    except PageNotAnInteger:
        carts = paginator.page(1)
        page = 1
    except EmptyPage:
        carts = paginator.page(paginator.num_pages)
        page = paginator.num_pages
    context = {
        "carts": carts,
        "pages": range(1, paginator.num_pages+1),
        "active_page": int(page),
    }
    return render(request, "myprofile/admin/orders.html", context)


@login_required
@user_passes_test(lambda u: u.is_staff or u.is_superuser)
def admin_order_details(request, id):
    cart = get_object_or_404(Cart, id=id)
    context = {
        "cart": cart,
    }
    return render(request, "myprofile/admin/order_details.html", context)


@csrf_exempt
def admin_order_approve(request):
    if request.method == "POST":
        user = request.user
        if not (user.is_staff or user.is_superuser):
            return http_response_400(
                "403",
                "permission denied"
            )
        if 'id' not in request.POST:
            return http_response_400(
                "404",
                "wrong params"
            )
        id = request.POST.get("id")
        if Cart.objects.filter(id=id).filter(status="PROGRESS").exists():
            cart = Cart.objects.get(id=id)
        else:
            return http_response_400("404", "cart with id %s doesn't exist or already approved" % id)
        cart.status = "CLOSED"
        cart.save()
        return HttpResponse(status=200, )
    else:
        get_400_response()


@login_required
@user_passes_test(lambda u: u.is_staff or u.is_superuser)
def admin_users(request):
    query = User.objects.all()
    if 'search' in request.GET:
        search = request.GET.get('search')
        query = query.filter(
            Q(username__icontains=search) |
            Q(first_name__icontains=search) |
            Q(email__icontains=search))
    paginator = Paginator(query.order_by("username"), 20)
    page = request.GET.get('page')
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
        page = 1
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
        page = paginator.num_pages
    context = {
        "users": users,
        "pages": range(1, paginator.num_pages + 1),
        "active_page": int(page),
    }
    return render(request, "myprofile/admin/users.html", context)


@csrf_exempt
def admin_user_grant(request):
    if request.method == "POST":
        user = request.user
        if not (user.is_staff or user.is_superuser):
            return http_response_400(
                "403",
                "permission denied"
            )
        if 'id' not in request.POST:
            return http_response_400(
                "404",
                "wrong params"
            )
        id = request.POST.get("id")
        try:
            user = User.objects.get(id=id)
        except:
            return http_response_400("404", "no user with id %s" % id)
        user.is_staff = True
        user.save()
        return HttpResponse(status=200, )
    else:
        get_400_response()
