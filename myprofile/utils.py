import json
import datetime
from django.http import HttpResponse


def http_response_200(out_dict):
    return HttpResponse(
        json.dumps(out_dict),
        content_type='application/json',
    )


def http_response_400(status, value):
    response = HttpResponse(
        json.dumps(
            {
                'status': status,
                'reason': value
            }),
        content_type='application/json',
    )
    response.status_code = 400
    return response


def post_400_response():
    response = HttpResponse(json.dumps(
        {
            'status': '400',
            'reason': 'Use GET request'}
    ),
        content_type='application/json'
    )
    response.status_code = 400
    return response


def get_400_response():
    response = HttpResponse(json.dumps(
        {
            'status': '400',
            'reason': 'Use POST request'}
    ),
        content_type='application/json'
    )
    response.status_code = 400
    return response


def js_data_to_python(jsdata):
    splitted = jsdata.split(jsdata[4])
    return datetime.date(int(splitted[0]), int(splitted[1]), int(splitted[2]))


def python_data_to_js_data(pydata):
    return pydata.strftime("%Y/%m/%d")


def python_data_to_normal(pydata):
    return pydata.strftime("%d/%m/%Y")


import urllib, urllib2, simplejson
from django.utils.encoding import smart_str


def get_lat_lng(location):
    # http://djangosnippets.org/snippets/293/
    # http://code.google.com/p/gmaps-samples/source/browse/trunk/geocoder/python/SimpleParser.py?r=2476
    # http://stackoverflow.com/questions/2846321/best-and-simple-way-to-handle-json-in-django
    # http://djangosnippets.org/snippets/2399/

    location = urllib.quote_plus(smart_str(location))
    url = 'http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false' % location
    response = urllib2.urlopen(url).read()
    result = simplejson.loads(response)
    if result['status'] == 'OK':
        lat = str(result['results'][0]['geometry']['location']['lat'])
        lng = str(result['results'][0]['geometry']['location']['lng'])
        return '%s,%s' % (lat, lng)
    else:
        return ''
