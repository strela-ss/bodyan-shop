# coding=utf-8
from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models
from imagekit.models import ImageSpecField
from pilkit.processors import ResizeToFill, ResizeToFit

BAND_STYLE = (
    ('METAL_CORE', 'Металкор'),
    ('ELECTRO_ROCK', 'Електронік рок'),
    ('LYRIC_ROCK', 'Лірик рок'),
    ('METAL', 'Метал'),
    ('POP_ROCK', 'Поп рок'),
    ('FOLK_ROCK', 'Фольк рок'),
    ('ALT_ROCK', 'Альтернативний рок'),
)


class City(models.Model):
    name = models.CharField(
        max_length=128,
        null=False,
        blank=False,
        verbose_name="Ім'я"
    )
    is_capital = models.BooleanField(
        default=False,
        verbose_name="Столиця"
    )

    def __unicode__(self):
        return "%s" % self.name

    class Meta:
        verbose_name = "Місто"
        verbose_name_plural = "Міста"


class Band(models.Model):
    name = models.CharField(
        null=False,
        blank=False,
        max_length=128,
        unique=True,
        verbose_name="Ім'я"
    )
    desc = models.TextField(
        null=True,
        blank=True,
        verbose_name="Опис"
    )
    style = models.CharField(
        null=False,
        blank=False,
        max_length=255,
        choices=BAND_STYLE,
        verbose_name="Стиль"
    )
    city = models.ForeignKey(
        City,
        on_delete=models.CASCADE,
        verbose_name="Місто"
    )
    price = models.IntegerField(
        null=False,
        blank=False,
        verbose_name="Ціна за годину"
    )
    youtube = models.CharField(
        null=True,
        blank=True,
        max_length=255,
        verbose_name="Посилання на відео youtube"
    )
    logo_full = models.ImageField(
        null=True,
        blank=True,
        upload_to='img/bands',
        verbose_name="Логотип",
        help_text="Бажаний розмір 970х500"
    )
    logo = ImageSpecField(
        source='logo_full',
        processors=[ResizeToFill(970, 500)],
        format='JPEG',
        options={'quality': 60}
    )
    logo_listing = ImageSpecField(
        source='logo_full',
        processors=[ResizeToFit(300, 300, mat_color="#000000")],
        format='JPEG',
        options={'quality': 60}
    )
    user = models.ForeignKey(
        User,
        null=False,
        blank=False,
    )

    def __unicode__(self):
        return "%s" % self.name

    class Meta:
        verbose_name = "Гурт"
        verbose_name_plural = "Гурти"


class Actor(models.Model):
    name = models.CharField(
        null=False,
        blank=False,
        max_length=128,
        unique=True,
        verbose_name="Ім'я"
    )
    desc = models.TextField(
        null=True,
        blank=True,
        verbose_name="Опис"
    )
    logo_full = models.ImageField(
        null=True,
        blank=True,
        upload_to='img/actors',
        verbose_name="Логотип"
    )
    logo = ImageSpecField(source='logo_full',
                          processors=[ResizeToFill(970, 500)],
                          format='JPEG',
                          options={'quality': 60})
    band = models.ForeignKey(
        Band,
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING,
        verbose_name="Гурт"
    )
    user = models.ForeignKey(
        User,
        null=False,
        blank=False,
    )

    class Meta:
        verbose_name = "Співак"
        verbose_name_plural = "Співаки"
