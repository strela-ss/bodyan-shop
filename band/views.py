from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from band.models import Band
from housing.models import Housing
from band.utils import *
from cart.models import Cart


def index(request):
    housings = Housing.objects.all().order_by('?')[:3]
    bands = list(Band.objects.all().order_by('?')[:5])
    context = {
        "band_active": bands[0],
        "bands": bands[1:],
        "housings": housings,
    }
    return render(request, "index.html", context)


def contact(request):
    return render(request, "contact.html", context={})


def listing(request):
    query = Band.objects.all()
    if 'search' in request.GET:
        search = request.GET.get('search')
        query = query.filter(
            Q(name__icontains=search))
    paginator = Paginator(query.order_by("name"), 9)
    page = request.GET.get('page')
    try:
        bands = paginator.page(page)
    except PageNotAnInteger:
        bands = paginator.page(1)
        page = 1
    except EmptyPage:
        bands = paginator.page(paginator.num_pages)
        page = paginator.num_pages
    context = {
        "bands": bands,
        "pages": range(1, paginator.num_pages+1),
        "active_page": int(page),
    }
    return render(request, "band/listing.html", context)


def details(request, id):
    band = get_object_or_404(Band, id=id)
    context = {
        "band": band,
    }
    return render(request, "band/details.html", context)


@csrf_exempt
def add_to_cart(request):
    if not request.user.is_authenticated():
        return http_response_400(
            "403",
            "Permission denied",
        )
    if request.method == "POST":
        user = request.user
        if user.cart_set.filter(status="PROGRESS").exists():
            return http_response_400("403", "cart in progress")
        elif user.cart_set.filter(status="OPEN").exists():
            cart = user.cart_set.get(status="OPEN")
        else:
            cart = Cart()
            cart.user = request.user
            cart.status = "OPEN"
            cart.save()
        if not 'id' in request.POST:
            return http_response_400(
                "404",
                "wrong params"
            )
        id = request.POST.get('id')
        try:
            band = Band.objects.get(id=id)
        except:
            return http_response_400(
                "404",
                "no band with id %s" % id,
            )
        if band not in cart.bands.all():
            cart.bands.add(band)
            return HttpResponse(
                status=200,
            )
        else:
            return http_response_400(
                "400",
                "band is already in the cart",
            )
    else:
        return get_400_response()
