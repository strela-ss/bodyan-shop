from django.contrib import admin
from .forms import CityForm, ActorForm, BandForm
from .models import *


class CityAdmin(admin.ModelAdmin):
    form = CityForm

    list_display = [
        'name', 'is_capital',
    ]

    search_fields = ('name',)
    ordering = ('name',)


class ActorAdmin(admin.ModelAdmin):
    form = ActorForm

    list_display = [
        'name', 'band',
    ]

    search_fields = ('name',)
    ordering = ('name',)


class BandAdmin(admin.ModelAdmin):
    form = BandForm

    list_display = [
        'name', 'style', 'city', 'price',
    ]

    search_fields = ('name',)
    ordering = ('name',)


admin.site.register(City, CityAdmin)
admin.site.register(Actor, ActorAdmin)
admin.site.register(Band, BandAdmin)
