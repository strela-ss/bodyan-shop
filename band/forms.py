from django import forms
from .models import *


class CityForm(forms.ModelForm):
    class Meta:
        model = City
        fields = ['name', 'is_capital']


class ActorForm(forms.ModelForm):
    class Meta:
        model = Actor
        fields = '__all__'
        exclude = ['logo']


class ActorUserForm(forms.ModelForm):
    class Meta:
        model = Actor
        fields = '__all__'
        exclude = ['logo', 'user']


class BandForm(forms.ModelForm):
    class Meta:
        model = Band
        fields = '__all__'
        exclude = ['logo']


class BandUserForm(forms.ModelForm):
    class Meta:
        model = Band
        fields = '__all__'
        exclude = ['logo', 'user']
