from django.conf.urls import url
import band.views

urlpatterns = [
    url(r'^$', band.views.listing, name="listing"),
    url(r'(?P<id>\d+)/', band.views.details, name='details'),
    url(r'add-to-cart/', band.views.add_to_cart, name='add-to-cart'),
]
