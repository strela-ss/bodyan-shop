# coding=utf-8
from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models
from imagekit.models import ImageSpecField
from pilkit.processors import ResizeToFill
from band.models import City
from myprofile.utils import get_lat_lng


HOUSING_TYPE = (
    ("ART_CAFE", "Арт кафе"),
    ("CLUB", "Клуб"),
    ("PUB", "Паб"),
    ("STADIUM", "Стадіон"),
    ("CONCERT_HALL", "Концертний зал"),
)
EQUIPMENT_TYPE = (
    ("NONE", "Без апаратури"),
    ("ACOUSTIC", "Для акустики"),
    ("5KWT", "До 5 кВт звуку"),
    ("5-10KWT", "Від 5 до 10 кВт звуку"),
    ("10+KWT", "10+ кВт звуку"),
)


class Housing(models.Model):
    name = models.CharField(
        null=False,
        blank=False,
        max_length=100,
        unique=True,
        verbose_name="Ім'я"
    )
    desc = models.TextField(
        null=True,
        blank=True,
        verbose_name="Опис"
    )
    type = models.CharField(
        choices=HOUSING_TYPE,
        max_length=100,
        null=False,
        blank=False,
        verbose_name="Тип проміщення"
    )
    price = models.IntegerField(
        null=False,
        blank=False,
        verbose_name="Ціна за годину оренди"
    )
    street = models.CharField(
        blank=True,
        null=False,
        default="",
        max_length=100,
        verbose_name="Вулиця"
    )
    number = models.CharField(
        blank=True,
        null=False,
        default="",
        max_length=10,
        verbose_name="Номер будівлі"
    )
    city = models.ForeignKey(
        City,
        null=False,
        blank=False,
        max_length=100,
        verbose_name="Місто",
    )
    region = models.CharField(
        blank=True,
        null=False,
        default="",
        max_length=100,
        verbose_name="Область",
    )
    location = models.CharField(
        blank=True,
        null=False,
        default="0,0",
        max_length=100,
    )
    capacity = models.IntegerField(
        null=False,
        blank=False,
        verbose_name="Місткість",
    )
    equipment = models.CharField(
        choices=EQUIPMENT_TYPE,
        max_length=100,
        null=False,
        blank=False,
        verbose_name="Обладнання",
    )
    logo_full = models.ImageField(
        null=True,
        blank=True,
        upload_to='img/housings',
        verbose_name="Логотип",
    )
    logo = ImageSpecField(source='logo_full',
                          processors=[ResizeToFill(970, 500)],
                          format='JPEG',
                          options={'quality': 60})
    user = models.ForeignKey(
        User,
        null=False,
        blank=False,
    )

    def address(self):
        return "%s %s %s %s" % (
            self.city,
            u", вул. " + self.street if self.street else "",
            u", буд. " + self.number if self.number else "",
            u", обл. " + self.region if self.region else "",
        )

    def save(self, *args, **kwargs):
        # Add + between fields with values:
        location = '+'.join(filter(None, (u"вул. ", self.street, self.number, self.city.name, self.region)))
        # Attempt to get latitude/longitude from Google Geocoder service v.3:
        self.location = get_lat_lng(location)
        super(Housing, self).save(*args, **kwargs)

    def __unicode__(self):
        return "%s: %s" % (self.get_type_display(), self.name)

    class Meta:
        verbose_name = "Концертна зала"
        verbose_name_plural = "Концертні зали"
