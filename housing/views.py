from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from cart.models import Cart
from housing.models import Housing
from housing.utils import get_400_response, http_response_400


def listing(request):
    query = Housing.objects.all()
    if 'search' in request.GET:
        search = request.GET.get('search')
        query = query.filter(
            Q(name__icontains=search))
    paginator = Paginator(query.order_by("name"), 5)
    page = request.GET.get('page')
    try:
        housings = paginator.page(page)
    except PageNotAnInteger:
        housings = paginator.page(1)
        page = 1
    except EmptyPage:
        housings = paginator.page(paginator.num_pages)
        page = paginator.num_pages
    context = {
        "housings": housings,
        "pages": range(1, paginator.num_pages+1),
        "active_page": int(page),
    }
    return render(request, "housing/listing.html", context)


def details(request, id):
    housing = get_object_or_404(Housing, id=id)
    context = {
        "housing": housing,
    }
    return render(request, "housing/details.html", context)


@csrf_exempt
def add_to_cart(request):
    if not request.user.is_authenticated():
        return http_response_400(
            "403",
            "Permission denied",
        )
    if request.method == "POST":
        user = request.user
        if user.cart_set.filter(status="PROGRESS").exists():
            return http_response_400("403", "cart in progress")
        elif user.cart_set.filter(status="OPEN").exists():
            cart = user.cart_set.get(status="OPEN")
        else:
            cart = Cart()
            cart.user = request.user
            cart.status = "OPEN"
            cart.save()
        if not 'id' in request.POST:
            return http_response_400(
                "404",
                "wrong params"
            )
        id = request.POST.get('id')
        try:
            housing = Housing.objects.get(id=id)
        except:
            return http_response_400(
                "404",
                "no housing with id %s" % id,
            )
        if housing not in cart.housings.all():
            cart.housings.add(housing)
            return HttpResponse(
                status=200,
            )
        else:
            return http_response_400(
                "400",
                "housing is already in the cart",
            )
    else:
        return get_400_response()
