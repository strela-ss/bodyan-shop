from django.contrib import admin
from housing.forms import HousingForm
from housing.models import *


class HousingAdmin(admin.ModelAdmin):
    form = HousingForm

    list_display = (
        "name",
        "price",
        "city",
        "location",
    )


admin.site.register(Housing, HousingAdmin)
