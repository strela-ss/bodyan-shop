from django.conf.urls import url
import housing.views


urlpatterns = [
    url(r'^$', housing.views.listing, name="listing"),
    url(r'(?P<id>\d+)/', housing.views.details, name='details'),
    url(r'add-to-cart/', housing.views.add_to_cart, name='add-to-cart'),
]
