# coding=utf-8
from django import forms
from housing.models import *


class HousingForm(forms.ModelForm):

    class Meta:
        model = Housing
        fields = '__all__'
        exclude = ('logo',)


class HousingUserForm(forms.ModelForm):

    class Meta:
        model = Housing
        fields = '__all__'
        exclude = ('user', 'logo',)


class HousingMapForm(forms.ModelForm):

    class Meta:
        model = Housing
        fields = '__all__'
        exclude = ('user', 'logo', 'location')
